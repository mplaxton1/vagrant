# Vagrant ROSi

## Pre-requisties

### Windows

Ensure Hyper-V is turned off in Windows Features otherwise you will get an error about VT-X.
A restart will be required if your Hyper-V is currently enabled.

### Vagrant

Install the latest version of Vagrant - currently I'm using 2.1.2
Ensure that you also install Virtual Box as part of the installation if you don't already have it.

A plugin is required to set the disksize of the installation. This can be installed with:
`vagrant plugin install vagrant-disksize`

### Virtual Box

Install the appropriate version of Guest Additions for your version of Virtual Box.

## Installation

The file `Vagrantfile` is used by Vagrant to control how VirtualBox will be used to
provision the VM.

The VM is configured via this file to use the following key settings:

- 16Gb of RAM `vb.memory`
- 100Gb of disk space `config.disksize.size = "100GB"`

You may need to update these settings if they are not appropriate for your machine.

The installation process takes about 30 mins to complete and is started with:
`vagrant up`

Once the installation completes you will need to reload Vagrant to see the Desktop using:
`vagrant reload`

### Monitor settings

For best Monitor setup use Full-Screen Mode once the VM has booted. Currently only one
monitor is enabled as multi-monitor seemed to crash when brought out of hibernation on
my machine.

### Starting the Vagrant Box

`vagrant up`

### Stopping the Vagrant Box

`vagrant halt`

### Git Setup

Although Git is installed it still needs to be configured by each user manually.

#### Git Configuration

Substitute `matt.plaxton` username/email with your username.

    git config --global user.name "Matt Plaxton"
    git config --global user.email "mplaxton@caesars.com"
    git config --global core.autocrlf input
    git config --global core.fscache true
    git config --global push.default simple

#### GitLab Key

`ssh-keygen`
Add contents of Public SSH Key file (i.e ~/.ssh/id_rsa.pub) to your User Profile in GitLab.

## General

The IP of the VM is always set to `192.168.99.253` and can be accessed via your Host machine.

The repo files are available on the VM under the `/vagrant` mount point.

# HELP!

If any of the provisioner scripts fail during installation then reload vagrant as described above and run the scripts manually from the VM via the `/vagrant` directory.

# TODO

- Create dedicated box with all tools provisioned to reduce provision time;
- Fix reload issue in Vagrantfile;
- Reduce/remove manual steps for Git user config;
