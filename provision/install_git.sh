#!/bin/sh
set -e

echo "Installing Git ..."

# Install Latest Version 8.x of Node.
sudo apt-get update
sudo apt-get install git-core -y

echo "Installed Git."
