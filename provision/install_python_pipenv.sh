#!/bin/sh
set -e

echo "Installing Pipenv ..."

# Install pip and pipenv
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.7 get-pip.py --no-warn-script-location
/home/vagrant/.local/bin/pip3 install pipenv --user --no-warn-script-location

# Update PATH
echo "export PATH=/home/vagrant/.local/bin:$PATH" \
>> /home/vagrant/.bashrc

echo "Installed Pipenv"
