#!/bin/sh
set -e

echo "Installing Ubuntu Desktop ..."

# Install LUbuntu and VirtualBox additions
sudo add-apt-repository multiverse
sudo apt-get update

# virtualbox-guest-dkms 
sudo DEBIAN_FRONTEND=noninteractive apt-get install lubuntu-desktop virtualbox-guest-utils virtualbox-guest-x11 -y

#sudo apt-get install -y xfce4 virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
# Permit anyone to start the GUI
#sudo sed -i 's/allowed_users=.*$/allowed_users=anybody/' /etc/X11/Xwrapper.config

# Turn off screen locking
sudo gsettings set org.gnome.desktop.screensaver lock-enabled false

echo "Installed Ubuntu Desktop."
