#!/bin/sh

echo "Installing Tools ..."

sudo apt-get update

# Install Visual Studio Code
sudo apt update
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y
sudo apt update
sudo apt install code -y

# Install Plugins
code --install-extension christian-kohler.npm-intellisense
code --install-extension christian-kohler.path-intellisense
code --install-extension dbaeumer.vscode-eslint
code --install-extension eamodio.gitlens
code --install-extension eg2.vscode-npm-script
code --install-extension ewnd9.open-node-modules
code --install-extension fknop.vscode-npm
code --install-extension HookyQR.beautify
code --install-extension humao.rest-client
code --install-extension jebbs.plantuml
code --install-extension kumar-harsh.graphql-for-vscode
code --install-extension Orta.vscode-jest
code --install-extension PeterJausovec.vscode-docker
code --install-extension shd101wyy.markdown-preview-enhanced
code --install-extension waderyan.nodejs-extension-pack
code --install-extension xabikos.JavaScriptSnippets

# Install Terminator
sudo apt-get install terminator -y

# Install IntelliJ Ultimate
sudo snap install intellij-idea-ultimate --classic

echo "Installed Tools."
