#!/bin/sh
set -e

echo "Installing Redis CLI ..."

wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make redis-cli
sudo cp src/redis-cli /usr/local/bin/

echo "Installed Redis CLI."
