#!/bin/sh
set -e

echo "Installing Yarn ..."

# Install Yarn Repo
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# Install Yarn (without recmommended Node)
sudo apt update
sudo apt install --no-install-recommends yarn -y

echo "Installed Yarn."
