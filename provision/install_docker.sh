#!/bin/sh
set -e

export DEV_DOCKER_VERSION=20.10.17~3-0~ubuntu-jammy
export DEV_DOCKER_COMPOSE_VERSION=1.22.0

# Docker
echo "Installing Docker Version: ${DEV_DOCKER_VERSION} ..."
sudo apt-get update
sudo apt-get install --yes apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get install --yes docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-compose
sudo chmod 666 /var/run/docker.sock
sudo usermod -aG docker vagrant
echo "Installed Docker Version: ${DEV_DOCKER_VERSION}"
