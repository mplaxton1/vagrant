#!/bin/sh
set -e

echo "Installing Python 3.7 ..."

sudo apt-get update
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update
sudo apt-get install python3.7 -y
# This breaks add-apt-repository.
# sudo rm /usr/bin/python3
# sudo ln -s /usr/bin/python3.7 /usr/bin/python3

# Essential libs
sudo apt-get install build-essential libpython3.7-dev -y

echo "Installed Python 3.7."
