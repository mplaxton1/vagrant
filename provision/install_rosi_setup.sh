#!/bin/sh
set -e

echo "Installing ROSi Setup ..."

# Install C++ for Node Kafka Modules.
sudo apt-get update
sudo apt-get install build-essential -y

echo "Installed ROSi Setup."
