#!/bin/sh
set -e

echo "Installing Java and Tools ..."

# Install JDK
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.10.2
. $HOME/.asdf/asdf.sh

asdf plugin add java
asdf install java temurin-17.0.3+7
asdf global java temurin-17.0.3+7
export JAVA_HOME=${asdf where java}

# Update Profile
sudo echo "export JAVA_HOME=`asdf where java`" >> ~vagrant/.bashrc

# sudo add-apt-repository ppa:openjdk-r/ppa -y
# sudo apt-get update
# sudo apt install openjdk-11-jdk -y

# Install Maven
sudo apt install maven -y

# Configure Maven Settings
mkdir /home/vagrant/.m2
cp /vagrant/provision/resources/maven/settings.xml /home/vagrant/.m2
sudo chmod -R 777 /home/vagrant/.m2
sudo chown -R vagrant /home/vagrant/.m2

echo "Installed Java and Tools"
