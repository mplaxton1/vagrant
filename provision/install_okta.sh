#!/bin/sh
set -e

echo "Installing Okta ..."

# Install AWS Cli
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

# Install Okta
wget https://github.com/segmentio/aws-okta/releases/download/v0.19.4/aws-okta_v0.19.4_amd64.deb
sudo dpkg -i aws-okta_v0.19.4_amd64.deb

# Install Kubectl
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install kubectl -y

# Install IAM Authenticator
curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator
mkdir -p /home/vagrant/tools
cp ./aws-iam-authenticator /home/vagrant/tools/aws-iam-authenticator
chmod -R 777 /home/vagrant/tools/aws-iam-authenticator
echo "export PATH=/home/vagrant/tools:$PATH" \
>> /home/vagrant/.bashrc

# Install AWS Config
mkdir /home/vagrant/.aws
cp /vagrant/provision/resources/aws/config /home/vagrant/.aws
sudo chmod -R 777 /home/vagrant/.aws
sudo chown -R vagrant /home/vagrant/.aws

echo "Installed Okta."
