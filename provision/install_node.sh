#!/bin/sh
set -e

echo "Installing Node ..."

# Install Latest Version 14.x of Node.
sudo apt-get update
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install nodejs -y

# Increase number of watches so that Nodemon can work correctly.
echo 524288 | sudo tee /proc/sys/fs/inotify/max_user_watches

echo "Installed Node."
