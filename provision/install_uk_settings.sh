#!/bin/sh
set -e

echo "Installing UK Time ..."
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/Europe/London /etc/localtime
echo "UK Time installed."

echo "Installing UK keyboard ..."
sudo cat <<EOF > /etc/default/keyboard
# KEYBOARD CONFIGURATION FILE
# Consult the keyboard(5) manual page.
XKBMODEL="microsoftoffice"
XKBLAYOUT="gb"
XKBVARIANT=""
XKBOPTIONS=""
BACKSPACE="guess"
EOF
echo "UK Keyboard installed"