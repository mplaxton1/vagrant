#!/bin/sh
set -e

echo "Installing William Hill HTTPS Certs ..."
export DOMAIN_NAME=docker-registry.prod.williamhill.plc
export TCP_PORT=443
 
openssl s_client -connect ${DOMAIN_NAME}:${TCP_PORT} -showcerts </dev/null 2>/dev/null | openssl x509 -outform PEM | sudo tee ${DOMAIN_NAME}.crt
sudo mkdir -p /etc/docker/certs.d/${DOMAIN_NAME}
sudo cp ${DOMAIN_NAME}.crt /etc/docker/certs.d/${DOMAIN_NAME}/.
sudo cp ${DOMAIN_NAME}.crt /usr/local/share/ca-certificates/.
sudo update-ca-certificates
echo "Installed William Hill HTTPS Certs."