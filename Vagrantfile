# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  #config.vm.box = "rosi"
  config.vm.box = "ubuntu/jammy64"

  # Requires PLUGIN, i.e vagrant plugin install vagrant-disksize
  config.disksize.size = "100GB"

  # Forward local SSH Key settings.
  config.ssh.forward_agent = true

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Fix IP and expose MySQL Port
  config.vm.network "private_network", ip: "192.168.99.253"
  config.vm.network "forwarded_port", guest: 3306, host: 3306
  config.vm.network "forwarded_port", guest: 2181, host: 2181
  config.vm.network "forwarded_port", guest: 9092, host: 9092
  config.vm.network "forwarded_port", guest: 9000, host: 9000
  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 8888, host: 8888

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    vb.name = "ROSi-22.04-v1"
    vb.gui = true
    #vb.memory = "12288"
    vb.memory = "24576"
    vb.cpus = 4

    # Use DNS via Host Machine
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]

    # Set number of monitors
    vb.customize ["modifyvm", :id, "--monitorcount", "1"]

    # Enabled Clipboard
    vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]

    # Enable full virtual memory and 3d acceleration of display.
    vb.customize ["modifyvm", :id, "--vram", "256"]
    vb.customize ["modifyvm", :id, "--accelerate3d", "on"]
    vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]


    # Enabling the I/O APIC is required for 64-bit guest operating systems, especially Windows Vista;
    # it is also required if you want to use more than one virtual CPU in a VM.
    vb.customize ["modifyvm", :id, "--ioapic", "on"]

    # Enable USB integration
    vb.customize ["modifyvm", :id, "--usb", "on"]
    vb.customize ["modifyvm", :id, "--usbehci", "on"]

    # Add Barcode Scanner USB
    # vb.customize ["usbfilter", "add", "0", "--target", :id, "--name", "Barcode Scanner", "--productid", 0001]
  end

  # Provision
  # NOTE: Python to be worked out for 2022
  #
  config.vm.provision "shell", path: "provision/install_uk_settings.sh"
  config.vm.provision "shell", path: "provision/install_desktop.sh"
  config.vm.provision "shell", path: "provision/install_docker.sh"
  config.vm.provision "shell", path: "provision/install_git.sh"
  config.vm.provision "shell", path: "provision/install_node.sh"
  config.vm.provision "shell", path: "provision/install_tools.sh"
  config.vm.provision "shell", path: "provision/install_rosi_setup.sh"  
  config.vm.provision "shell", path: "provision/install_java.sh"
  # config.vm.provision "shell", path: "provision/install_python.sh"
  # config.vm.provision "shell", privileged: false, path: "provision/install_python_pipenv.sh"
  config.vm.provision "shell", path: "provision/install_yarn.sh"

  # A vagrant reload is required after a Desktop Install

end
